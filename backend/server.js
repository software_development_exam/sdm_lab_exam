const express = require('express')
const app = express()
const db =  require('./db')
const util = require('./utils')

app.use(express.json())

app.get('/', (request, response)=>{
    const {title} = request.body
    const statement = `select movie_title, movie_release_date, movie_time, director_name
                        from movie where movie_title = '${title}'`
    db.execute(statement, (error, result)=>{
        console.log(result);
        response.send(util.createResult(error, result))
    })
})

app.post('/', (request, response)=>{
    const {title, releaseDate, time, directorName} = request.body
    const statement = `insert into movie (movie_title, movie_release_date, movie_time, director_name) values('${title}', '${releaseDate}', ${time}, '${directorName}')`

    db.execute(statement, (error, result)=>{
        console.log(result);
        response.send(util.createResult(error, result))
    })
})

app.put('/:id', (request, response)=>{
    const {id} = request.params
    const { releaseDate, time} = request.body
    const statement = `update movie set movie_release_date = '${releaseDate}', movie_time = ${time} where movie_id = ${id}`

    db.execute(statement, (error, result)=>{
        if(result['affectedRows'] === 0){
            response.send(util.createResult('movie not found', null))
            return
        }
        console.log(result);
        response.send(util.createResult(error, result))
    })
})

app.delete('/:id', (request, response)=>{
    const {id} = request.params
    const statement = `delete from movie where movie_id = ${id}`

    db.execute(statement, (error, result)=>{
        if(result['affectedRows'] === 0){
            response.send(util.createResult('movie not found', null))
            return
            
        }
        console.log(result);
        response.send(util.createResult(error, result))
    })
})



app.listen(4000, ()=>{
    console.log('server started at port 4000');
})