const mysql =  require('mysql2')

const pool = mysql.createPool({
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'mydb',
    port: 3306,
    connectionLimit: 10,
    waitForConnections: true,
})

module.exports = pool