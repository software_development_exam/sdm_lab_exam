create table movie(
    movie_id INTEGER PRIMARY KEY auto_increment,
    movie_title VARCHAR(80),
    movie_release_date DATE,
    movie_time FLOAT,
    director_name VARCHAR(80)
    );

insert into movie (movie_title, movie_release_date, movie_time, director_name) values('freedom', '2021-5-10', 2.30, 'James Gunn');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values('Inception', '2005-11-21', 2.00, 'Cristopher Nolan');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values('Spiderman', '2006-1-4', 1.50, 'sam raimie');

